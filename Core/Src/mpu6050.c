#include "mpu6050.h"
#include "myiic.h"
#include "usbd_cdc_if.h"
#include "imu.h"

uint8_t temp_key[4];

void MPU6050ReadData(short *mpudata);

/**
  * @brief   写数据到MPU6050寄存器
  * @param   reg_add:寄存器地址
	* @param	 reg_data:要写入的数据
  * @retval  
  */
void MPU6050_WriteReg(uint8_t reg_add, uint8_t reg_dat)
{
  Sensor_write(reg_add, reg_dat);
}

/**
  * @brief   从MPU6050寄存器读取数据
  * @param   reg_add:寄存器地址
	* @param	 Read：存储数据的缓冲区
	* @param	 num：要读取的数据量
  * @retval  
  */
void MPU6050_ReadData(uint8_t reg_add, unsigned char *Read, uint8_t num)
{
  Sensor_Read(reg_add, Read, num);
}
#define M_PI		3.14159265358979323846
void mpu6050_transmit(void)
{
    static float old_yaw;
    static T_float_angle angle;
    static int16_t mpu6050_read_data[6];
    static int32_t temp_data32[4];
    static int16_t acc[3],gyro[3];
    
    MPU6050ReadData(mpu6050_read_data);
    acc[0] = mpu6050_read_data[0];
    acc[1] = mpu6050_read_data[1];
    acc[2] = mpu6050_read_data[2];
    
    gyro[0] = mpu6050_read_data[3];
    gyro[1] = mpu6050_read_data[4];
    gyro[2] = mpu6050_read_data[5];
    
    prepareData((T_int16_xyz *)acc, (T_int16_xyz *)acc);
	IMUupdate((T_int16_xyz *)gyro, (T_int16_xyz *)acc, &angle);
    
    //angle.yaw += 0.01145935;
    
    temp_data32[0] =((int32_t)(angle.rol*100));
	temp_data32[1] =((int32_t)(angle.pit*100));
	temp_data32[2] =((int32_t)(angle.yaw*100));
    
    *(((uint8_t *)(&temp_data32[3]))+0) = temp_key[0];
    *(((uint8_t *)(&temp_data32[3]))+1) = temp_key[1];
    *(((uint8_t *)(&temp_data32[3]))+2) = temp_key[2];
    *(((uint8_t *)(&temp_data32[3]))+3) = temp_key[3];
    
//  printf("%d\t%d\t%d\t%d\t%d\t%d\r\n",  mpu6050_read_data[0],
//                                        mpu6050_read_data[1],
//                                        mpu6050_read_data[2],
//                                        mpu6050_read_data[3],
//                                        mpu6050_read_data[4],
//                                        mpu6050_read_data[5]);
                                  
    //printf("%0.2f\t%0.2f\t%0.2f\r\n",angle.pit,angle.rol,angle.yaw);
   
    //printf("%0.8f\r\n",angle.yaw-old_yaw);
    //old_yaw = angle.yaw;
    
      CDC_Transmit_FS((uint8_t*)temp_data32, 16);

    
}
/**
  * @brief   初始化MPU6050芯片
  * @param   
  * @retval  
  */
void MPU6050_Init(void)
{

  IMU_I2C_Init();

  MPU6050_WriteReg(MPU6050_RA_PWR_MGMT_1, 0x80); //复位MPU6050
  HAL_Delay(100);
  MPU6050_WriteReg(MPU6050_RA_PWR_MGMT_1, 0x00); //解除休眠状态
  MPU6050_WriteReg(MPU6050_RA_SMPLRT_DIV, 0x07); //陀螺仪采样率
  MPU6050_WriteReg(MPU6050_RA_CONFIG, 0x06);
  MPU6050_WriteReg(MPU6050_RA_ACCEL_CONFIG, 0x10); //配置加速度传感器工作在16G模式
  MPU6050_WriteReg(MPU6050_RA_GYRO_CONFIG, 0x18);  //陀螺仪自检及测量范围，典型值：0x18(不自检，2000deg/s)
  HAL_Delay(50);

  if (MPU6050ReadID() == 1)
  {
    printf("[MPU6050] [Init Success]\r\n");
  }
  else
  {
    printf("[MPU6050] [Init Fail]\r\n");
  }
  
}

/**
  * @brief   读取MPU6050的ID
  * @param   
  * @retval  正常返回1，异常返回0
  */
uint8_t MPU6050ReadID(void)
{
  unsigned char Re = 0;
  MPU6050_ReadData(MPU6050_RA_WHO_AM_I, &Re, 1); //读器件地址
  if (Re != 0x68)
  {
    printf("[MPU6050][ID ERRORR]\r\n");
    return 0;
  }
  else
  {
    //printf("MPU6050 ID = %d\r\n", Re);
    return 1;
  }
}

void MPU6050ReadData(int16_t *mpudata)
{
  uint8_t buf[14];

  MPU6050_ReadData(MPU6050_ACC_OUT, buf, 14);

  mpudata[0] = (buf[0] << 8) | buf[1];
  mpudata[1] = (buf[2] << 8) | buf[3];
  mpudata[2] = (buf[4] << 8) | buf[5];

  mpudata[3] = (buf[8] << 8) | buf[9];
  mpudata[4] = (buf[10] << 8) | buf[11];
  mpudata[5] = (buf[12] << 8) | buf[13];
}

/**
  * @brief   读取MPU6050的加速度数据
  * @param   
  * @retval  
  */
void MPU6050ReadAcc(short *accData)
{
  uint8_t buf[6];
  MPU6050_ReadData(MPU6050_ACC_OUT, buf, 6);
  accData[0] = (buf[0] << 8) | buf[1];
  accData[1] = (buf[2] << 8) | buf[3];
  accData[2] = (buf[4] << 8) | buf[5];
}

/**
  * @brief   读取MPU6050的角加速度数据
  * @param   
  * @retval  
  */
void MPU6050ReadGyro(short *gyroData)
{
  uint8_t buf[6];
  MPU6050_ReadData(MPU6050_GYRO_OUT, buf, 6);
  gyroData[0] = (buf[0] << 8) | buf[1];
  gyroData[1] = (buf[2] << 8) | buf[3];
  gyroData[2] = (buf[4] << 8) | buf[5];
}

/**
  * @brief   读取MPU6050的原始温度数据
  * @param   
  * @retval  
  */
void MPU6050ReadTemp(short *tempData)
{
  uint8_t buf[2];
  MPU6050_ReadData(MPU6050_RA_TEMP_OUT_H, buf, 2); //读取温度值
  *tempData = (buf[0] << 8) | buf[1];
}

/**
  * @brief   读取MPU6050的温度数据，转化成摄氏度
  * @param   
  * @retval  
  */
void MPU6050_ReturnTemp(float *Temperature)
{
  short temp3;
  uint8_t buf[2];

  MPU6050_ReadData(MPU6050_RA_TEMP_OUT_H, buf, 2); //读取温度值
  temp3 = (buf[0] << 8) | buf[1];
  *Temperature = ((double)temp3 / 340.0) + 36.53;
}
